FROM debian:10

MAINTAINER Tchit10 "tchit10@outlook.com"

RUN apt-get update && apt-get install -y \
    ansible \
    openssh-client \
 && rm -rf /var/lib/apt/lists/*
